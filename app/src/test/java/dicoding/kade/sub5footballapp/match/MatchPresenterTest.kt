package dicoding.kade.sub5footballapp.match

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import dicoding.kade.sub5footballapp.api.ApiInterface
import dicoding.kade.sub5footballapp.model.Schedule
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MatchPresenterTest {

    private val okHttpClient = OkHttpClient()
    private val webServer = MockWebServer()
    private lateinit var service: ApiInterface

    @Before
    fun setUp(){
        val retrofit = Retrofit.Builder()
                .baseUrl(webServer.url(""))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        service = retrofit.create(ApiInterface::class.java)
    }

    @Test
    fun return_schedules_on_200_http_response() {
        val expectedSchedules =
                Schedule("Bournemouth","Southampton",null,null,
                        "2018-10-20","14:00:00+00:00","134301","134778","576550")
        val response = service.getSchedule("eventsnextleague.php","4328").execute()
        val actualSchedules = response.body()!!.schedules!![0]

        assertTrue(response.isSuccessful)
        assertEquals(expectedSchedules,actualSchedules)
    }

    @Test
    fun return_schedules_on_non_200_http_response() {
        val response = service.getSchedule("","").execute()

        assertFalse(response.isSuccessful)

    }
}