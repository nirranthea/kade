package dicoding.kade.sub5footballapp.utils

import org.junit.Test

import org.junit.Assert.*

class UtilKtTest {

    //TODO : Unit Testing - convertDate() function
    @Test
    fun testConvertDate() {

        //Case - 1 Normal convert to GMT+7
        val strDateAndTime = "2018-09-29 13:00:00+00:00"
        assertEquals("Sat, 29 Sep 2018 20:00", convertDateAndTime(strDateAndTime))

        //Case 2 - Leap year convert to GMT+7
        val leapDateAndTime = "2016-02-29 10:30:00+00:00"
        assertEquals("Mon, 29 Feb 2016 17:30", convertDateAndTime(leapDateAndTime))

        //Case 3 - become next day convert to GMT+7
        val tomorrowDateAndTime = "2018-10-14 19:00:00+00:00"
        assertEquals("Mon, 15 Oct 2018 02:00", convertDateAndTime(tomorrowDateAndTime))

    }
}