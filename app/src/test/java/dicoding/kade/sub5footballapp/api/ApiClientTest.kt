package dicoding.kade.sub5footballapp.api

import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*

class ApiClientTest {

    //TODO : Unit Testing - ensure get() method is call within ApiClient
    @Test
    fun testGet() {
        val apiClient = mock(ApiClient::class.java)
        apiClient.get()
        Mockito.verify(apiClient).get()
    }

}