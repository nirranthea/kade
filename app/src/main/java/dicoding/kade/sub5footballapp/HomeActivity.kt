package dicoding.kade.sub5footballapp

import android.content.res.ColorStateList
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.LinearLayout
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub5footballapp.favorite.FavoriteFragment
import dicoding.kade.sub5footballapp.match.ScheduleFragment
import dicoding.kade.sub5footballapp.team.TeamFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.design.bottomNavigationView

class HomeActivity : AppCompatActivity() {

    private lateinit var ankoHomeUI: HomeActivityUI
    private val MENU_MATCH = 1
    private val MENU_TEAM = 2
    private val MENU_FAVORITE = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ankoHomeUI = HomeActivityUI()
        ankoHomeUI.setContentView(this)

        supportActionBar?.title = "Football App"

        //TODO : initialize BottomNavigation
        ankoHomeUI.bottomNavigation.menu
                .add(Menu.NONE,MENU_MATCH, Menu.NONE,"Jadwal")
                .setIcon(R.drawable.ic_baseline_calendar)
        ankoHomeUI.bottomNavigation.menu
                .add(Menu.NONE,MENU_TEAM, Menu.NONE,"Team")
                .setIcon(R.drawable.ic_teams)
        ankoHomeUI.bottomNavigation.menu
                .add(Menu.NONE, MENU_FAVORITE, Menu.NONE, "Favorites")
                .setIcon(R.drawable.ic_favourites)

        ankoHomeUI.bottomNavigation.setOnNavigationItemSelectedListener {
            item ->
            when (item.itemId){
                MENU_MATCH -> {
                    Log.d("Fragment","MENU_MATCH")
                    loadScheduleFragment(savedInstanceState)
                }
                MENU_TEAM -> {
                    Log.d("Fragment","MENU_TEAM")
                    loadTeamFragment(savedInstanceState)
                }
                MENU_FAVORITE -> {
                    Log.d("Fragment","MENU_FAVORITE")
                    loadFavoriteFragment(savedInstanceState)
                }
            }
            true
        }
        ankoHomeUI.bottomNavigation.selectedItemId = MENU_MATCH

    }

    private fun loadTeamFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null){
            Log.d("Fragment","loadTeamFragment")
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, TeamFragment(), TeamFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadScheduleFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null){
            Log.d("Fragment","loadScheduleFragment")
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, ScheduleFragment(), ScheduleFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadFavoriteFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null){
            Log.d("Fragment","loadFavoriteScheduleFragment")
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, FavoriteFragment(), FavoriteFragment::class.java.simpleName)
                    .commit()
        }
    }

    //TODO: ANKO-layout for HomeActivity
    class HomeActivityUI : AnkoComponent<HomeActivity>{
        lateinit var bottomNavigation: BottomNavigationView

        override fun createView(ui: AnkoContext<HomeActivity>): View = with(ui){
            verticalLayout {

                frameLayout {
                    id = R.id.main_container
                }.lparams(width = LinearLayout.LayoutParams.MATCH_PARENT, height = matchParent, weight = 1F)

                view {
                    setBackgroundResource(R.drawable.shadow)
                }.lparams(width = matchParent, height = dip(4f)) {

                }

                bottomNavigation = bottomNavigationView{
                    id = R.id.bottom_navigation

                    val states = arrayOf(
                            intArrayOf(android.R.attr.state_checked),
                            intArrayOf(android.R.attr.state_enabled)
                    )
                    val colors = intArrayOf(Color.RED, Color.DKGRAY)
                    val colorStateList = ColorStateList(states, colors)
                    itemTextColor = colorStateList
                    itemIconTintList = colorStateList

                }.lparams(width = LinearLayout.LayoutParams.MATCH_PARENT)
            }
        }
    }


}
