package dicoding.kade.sub5footballapp.model

import com.google.gson.annotations.SerializedName

data class Player(

        @SerializedName("idPlayer")
        var playerId: String? = null,

        @SerializedName("strPlayer")
        var playerName: String? = null,

        @SerializedName("strNationality")
        var playerNationality: String? = null,

        @SerializedName("strPosition")
        var playerPosition: String? = null,

        @SerializedName("strHeight")
        var playerHeight: String? = null,

        @SerializedName("strWeight")
        var playerWeight: String? = null,

        @SerializedName("strCutout")
        var playerProfilePicture: String? = null,

        @SerializedName("strThumb")
        var playerThumbnail: String? = null,

        @SerializedName("strFanart1")
        var playerFanart: String? = null,

        @SerializedName("strDescriptionEN")
        var playerDescription: String? = null
)