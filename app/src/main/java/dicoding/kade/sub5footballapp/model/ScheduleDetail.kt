package dicoding.kade.sub5footballapp.model

import com.google.gson.annotations.SerializedName

data class ScheduleDetail(

        @SerializedName("strTeamBadge")
        var teamLogo: String? = null

)