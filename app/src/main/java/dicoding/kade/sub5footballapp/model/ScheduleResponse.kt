package dicoding.kade.sub5footballapp.model

import com.google.gson.annotations.SerializedName

data class ScheduleResponse(

        @SerializedName("events")
        var schedules: List<Schedule>? = null

)