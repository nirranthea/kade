package dicoding.kade.sub5footballapp.model

import com.google.gson.annotations.SerializedName

data class MatchResponse(

        @SerializedName("event")
        var schedules: List<Schedule>? = null

)