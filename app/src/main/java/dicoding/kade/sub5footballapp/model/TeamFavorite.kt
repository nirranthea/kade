package dicoding.kade.sub5footballapp.model

data class TeamFavorite (val id: Long?,
                         val teamId: String?,
                         val teamName: String?,
                         val teamBadge: String?,
                         val teamFormedYear: String?,
                         val teamStadium: String?,
                         val teamDescription: String?) {

    companion object {
        const val TABLE_FAVORITE_TEAM: String = "TABLE_FAVORITE_TEAM"
        const val ID: String = "ID"
        const val TEAM_ID: String = "TEAM_ID"
        const val TEAM_NAME: String = "TEAM_NAME"
        const val TEAM_BADGE: String = "TEAM_BADGE"
        const val TEAM_FORMEDYEAR: String = "TEAM_FORMEDYEAR"
        const val TEAM_STADIUM: String = "TEAM_STADIUM"
        const val TEAM_DESCRIPTION: String = "TEAM_DESCRIPTION"
    }
}