package dicoding.kade.sub5footballapp.model

data class ScheduleFavorite(val id: Long?,
                            val eventId: String?,
                            val teamHomeId: String?,
                            val teamAwayId: String?,
                            val teamHomeName: String?,
                            val teamAwayName: String?,
                            val scoreHome: Int?,
                            val scoreAway: Int?,
                            val matchDate: String?,
                            val matchTime: String?,
                            val mode: String?,
                            val goalsHome: String?,
                            val goalsAway: String?,
                            val shotsHome: Int?,
                            val shotsAway: Int?,
                            val redCardsHome: String?,
                            val redCardsAway: String?,
                            val yellowCardsHome: String?,
                            val yellowCardsAway: String?,
                            val goalKeeperHome: String?,
                            val goalKeeperAway: String?,
                            val defenseHome: String?,
                            val defenseAway: String?,
                            val midfieldHome: String?,
                            val midfieldAway: String?,
                            val forwardHome: String?,
                            val forwardAway: String?
                            ) {

    companion object {
        const val TABLE_FAVORITE_MATCH: String = "TABLE_FAVORITE_MATCH"
        const val ID: String = "ID"
        const val EVENT_ID: String = "EVENT_ID"
        const val TEAM_HOME_ID: String = "TEAM_HOME_ID"
        const val TEAM_AWAY_ID: String = "TEAM_AWAY_ID"
        const val TEAM_HOME_NAME: String = "TEAM_HOME_NAME"
        const val TEAM_AWAY_NAME: String = "TEAM_AWAY_NAME"
        const val SCORE_HOME: String = "SCORE_HOME"
        const val SCORE_AWAY: String = "SCORE_AWAY"
        const val MATCH_DATE: String = "MATCH_DATE"
        const val MATCH_TIME: String = "MATCH_TIME"
        const val MODE: String = "MODE"
        const val GOALS_HOME: String = "GOALS_HOME"
        const val GOALS_AWAY: String = "GOALS_AWAY"
        const val SHOTS_HOME: String = "SHOTS_HOME"
        const val SHOTS_AWAY: String = "SHOTS_AWAY"
        const val REDCARDS_HOME: String = "REDCARDS_HOME"
        const val REDCARDS_AWAY: String = "REDCARDS_AWAY"
        const val YELLOWCARDS_HOME: String = "YELLOWCARDS_HOME"
        const val YELLOWCARDS_AWAY: String = "YELLOWCARDS_AWAY"
        const val GOALKEEPER_HOME: String = "GOALKEEPER_HOME"
        const val GOALKEEPER_AWAY: String = "GOALKEEPER_AWAY"
        const val DEFENSE_HOME: String = "DEFENSE_HOME"
        const val DEFENSE_AWAY: String = "DEFENSE_AWAY"
        const val MIDFIELD_HOME: String = "MIDFIELD_HOME"
        const val MIDFIELD_AWAY: String = "MIDFIELD_AWAY"
        const val FORWARD_HOME: String = "FORWARD_HOME"
        const val FORWARD_AWAY: String = "FORWARD_AWAY"
    }
}