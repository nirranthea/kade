package dicoding.kade.sub5footballapp.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FootballApp.db", null, 1) {

    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null){
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        //Here you create tables
        db.createTable(ScheduleFavorite.TABLE_FAVORITE_MATCH, true,
                ScheduleFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ScheduleFavorite.EVENT_ID to TEXT + UNIQUE,
                ScheduleFavorite.TEAM_HOME_ID to TEXT + UNIQUE,
                ScheduleFavorite.TEAM_AWAY_ID to TEXT + UNIQUE,
                ScheduleFavorite.TEAM_HOME_NAME to TEXT,
                ScheduleFavorite.TEAM_AWAY_NAME to TEXT,
                ScheduleFavorite.SCORE_HOME to INTEGER,
                ScheduleFavorite.SCORE_AWAY to INTEGER,
                ScheduleFavorite.MATCH_DATE to TEXT,
                ScheduleFavorite.MATCH_TIME to TEXT,
                ScheduleFavorite.MODE to TEXT,
                ScheduleFavorite.GOALS_HOME to TEXT,
                ScheduleFavorite.GOALS_AWAY to TEXT,
                ScheduleFavorite.SHOTS_HOME to INTEGER,
                ScheduleFavorite.SHOTS_AWAY to INTEGER,
                ScheduleFavorite.REDCARDS_HOME to TEXT,
                ScheduleFavorite.REDCARDS_AWAY to TEXT,
                ScheduleFavorite.YELLOWCARDS_HOME to TEXT,
                ScheduleFavorite.YELLOWCARDS_AWAY to TEXT,
                ScheduleFavorite.GOALKEEPER_HOME to TEXT,
                ScheduleFavorite.GOALKEEPER_AWAY to TEXT,
                ScheduleFavorite.DEFENSE_HOME to TEXT,
                ScheduleFavorite.DEFENSE_AWAY to TEXT,
                ScheduleFavorite.MIDFIELD_HOME to TEXT,
                ScheduleFavorite.MIDFIELD_AWAY to TEXT,
                ScheduleFavorite.FORWARD_HOME to TEXT,
                ScheduleFavorite.FORWARD_AWAY to TEXT )

        db.createTable(TeamFavorite.TABLE_FAVORITE_TEAM, true,
                TeamFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TeamFavorite.TEAM_ID to TEXT + UNIQUE,
                TeamFavorite.TEAM_NAME to TEXT,
                TeamFavorite.TEAM_BADGE to TEXT,
                TeamFavorite.TEAM_FORMEDYEAR to TEXT,
                TeamFavorite.TEAM_STADIUM to TEXT,
                TeamFavorite.TEAM_DESCRIPTION to TEXT )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //Here you can update tables, as usual
        db.dropTable(ScheduleFavorite.TABLE_FAVORITE_MATCH, true)
        db.dropTable(TeamFavorite.TABLE_FAVORITE_TEAM, true)
    }

}

//Access property for context
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)