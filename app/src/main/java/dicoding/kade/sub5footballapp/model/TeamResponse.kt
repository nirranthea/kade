package dicoding.kade.sub5footballapp.model

data class TeamResponse (
        val teams: List<Team>
)