package dicoding.kade.sub5footballapp.model


import com.google.gson.annotations.SerializedName

data class Schedule(

        @SerializedName("strHomeTeam")
        var teamHome: String? = null,

        @SerializedName("strAwayTeam")
        var teamAway: String? = null,

        @SerializedName("intHomeScore")
        var scoreHome: Int? = null,

        @SerializedName("intAwayScore")
        var scoreAway: Int? = null,

        @SerializedName("dateEvent")
        var date: String? = null,

        @SerializedName("strTime")
        var time: String? = null,

        @SerializedName("idHomeTeam")
        var teamHomeId: String? = null,

        @SerializedName("idAwayTeam")
        var teamAwayId: String? = null,

        @SerializedName("idEvent")
        var eventId: String? = null,

        @SerializedName("strHomeGoalDetails")
        var goalsHome: String? = null,

        @SerializedName("strAwayGoalDetails")
        var goalsAway: String? = null,

        @SerializedName("intHomeShots")
        var shotsHome: Int? = null,

        @SerializedName("intAwayShots")
        var shotsAway: Int? = null,

        @SerializedName("strHomeRedCards")
        var redCardsHome: String? = null,

        @SerializedName("strAwayRedCards")
        var redCardsAway: String? = null,

        @SerializedName("strHomeYellowCards")
        var yellowCardsHome: String? = null,

        @SerializedName("strAwayYellowCards")
        var yellowCardsAway: String? = null,

        @SerializedName("strHomeLineupGoalkeeper")
        var goalKeeperHome: String? = null,

        @SerializedName("strAwayLineupGoalkeeper")
        var goalKeeperAway: String? = null,

        @SerializedName("strHomeLineupDefense")
        var defenseHome: String? = null,

        @SerializedName("strAwayLineupDefense")
        var defenseAway: String? = null,

        @SerializedName("strHomeLineupMidfield")
        var midfieldHome: String? = null,

        @SerializedName("strAwayLineupMidfield")
        var midfieldAway: String? = null,

        @SerializedName("strHomeLineupForward")
        var forwardHome: String? = null,

        @SerializedName("strAwayLineupForward")
        var forwardAway: String? = null

)
