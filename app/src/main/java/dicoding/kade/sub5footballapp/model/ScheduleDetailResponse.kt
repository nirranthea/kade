package dicoding.kade.sub5footballapp.model

import com.google.gson.annotations.SerializedName

data class ScheduleDetailResponse(

        @SerializedName("teams")
        var scheduledetails: List<ScheduleDetail>

)
