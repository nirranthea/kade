package dicoding.kade.sub5footballapp.api

import dicoding.kade.sub5footballapp.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiInterface {

    @GET("https://www.thesportsdb.com/api/v1/json/1/{schedule_id}")
    fun getSchedule(@Path("schedule_id") schedule_id: String,
                    @Query("id") id: String)
            : Call<ScheduleResponse>

    @GET("https://www.thesportsdb.com/api/v1/json/1/lookupteam.php/")
    fun getTeamDetail(@Query("id") teamId: String)
            : Call<ScheduleDetailResponse>

    @GET("https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php/")
    fun getTeam(@Query("id") teamId: String)
            : Call<TeamResponse>

    @GET("https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php/")
    fun getPlayer(@Query("id") teamId: String)
            : Call<PlayerResponse>

    @GET("https://www.thesportsdb.com/api/v1/json/1/searchteams.php/")
    fun searchTeam(@Query("t") teamName: String)
            :Call<TeamResponse>

    @GET("https://www.thesportsdb.com/api/v1/json/1/searchevents.php/")
    fun searchMatch(@Query("e") matchName: String)
            :Call<MatchResponse>
}