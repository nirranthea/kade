package dicoding.kade.sub5footballapp.team

import android.util.Log
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.Team
import dicoding.kade.sub5footballapp.model.TeamResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TeamPresenter (private val view: TeamView,
                     private val apiClient: ApiClient) {

    fun getTeamList(leagueName: String?){
        view.showLoading()

        val leagueId: String = when (leagueName){
            "English Premier League" -> "4328"
            "German Bundesliga" -> "4331"
            "Italian Serie A" -> "4332"
            "Spanish La Liga" -> "4335"
            else -> "4328"
        }

        val cek: Call<TeamResponse> = apiClient.get().getTeam(leagueId)
        cek.enqueue(object : Callback<TeamResponse> {
            override fun onResponse(call: Call<TeamResponse>, response: Response<TeamResponse>) {
                Log.d("Retrofit", "success")
                if (response.isSuccessful && response.body() != null) {
                    Log.d("Retrofit2", response.body().toString())
                    val listResponse: List<Team>? = response.body()!!.teams
                    Log.d("Retrofit3", listResponse.toString())
                    view.hideLoading()
                    if (listResponse != null) view.showTeamList(listResponse)
                }
            }

            override fun onFailure(call: Call<TeamResponse>?, t: Throwable?) {
                Log.d("Retrofit", "failure : " + t?.message)
                view.hideLoading()
            }
        })
    }
}