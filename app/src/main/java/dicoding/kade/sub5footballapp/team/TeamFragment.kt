package dicoding.kade.sub5footballapp.team


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.display_search
import dicoding.kade.sub2footballmatchschedule.R.menu.menu_search
import dicoding.kade.sub5footballapp.search.SearchActivity
import dicoding.kade.sub5footballapp.adapter.TeamAdapter
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.detail.DetailTeamActivity
import dicoding.kade.sub5footballapp.model.Team
import dicoding.kade.sub5footballapp.utils.invisible
import dicoding.kade.sub5footballapp.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class TeamFragment : Fragment(), AnkoComponent<Context>, TeamView {

    private lateinit var listTeam: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var spinner: Spinner

    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var presenter: TeamPresenter
    private lateinit var adapter: TeamAdapter
    private var leagueName: String = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val spinnerItems = resources.getStringArray(R.array.league_name)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter

        adapter = TeamAdapter(teams){
            ctx.startActivity<DetailTeamActivity>("team_Id" to "${it.teamId}",
                    "team_name" to "${it.teamName}",
                    "team_badge" to "${it.teamBadge}",
                    "team_desc" to "${it.teamDescription}",
                    "team_stadium" to "${it.teamStadium}",
                    "team_year" to "${it.teamFormedYear}")
        }
        listTeam.adapter = adapter

        val request = ApiClient()
        presenter = TeamPresenter(this, request)
        presenter.getTeamList(leagueName)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                presenter.getTeamList(leagueName)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                leagueName = resources.getStringArray(R.array.league_name)[0]
            }

        }

        swipeRefresh.onRefresh {
            presenter.getTeamList(leagueName)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("Fragment","onCreateView TeamFragment")
        //TODO : enable optionsMenu in toolbar
        setHasOptionsMenu(true)
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View =with(ui){

        //TODO: ANKO-layout
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            spinner = spinner()
            view {
                setBackgroundResource(R.drawable.shadow)
            }.lparams(width = matchParent, height = dip(4f)) {

            }
            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {

                    }.lparams {
                        centerHorizontally()
                    }
                }
            }
        }

    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showTeamList(data: List<Team>) {
        swipeRefresh.isRefreshing = false
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
    }

    //TODO : Inflate menu contains Search
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(menu_search, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            display_search -> {
                Log.d("SearchTest","menu_search clicked")
                startActivity<SearchActivity>(
                        "fragment_type" to "TEAM_FRAGMENT"
                )
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
