package dicoding.kade.sub5footballapp.team

import dicoding.kade.sub5footballapp.model.Team

interface TeamView {

    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)

}