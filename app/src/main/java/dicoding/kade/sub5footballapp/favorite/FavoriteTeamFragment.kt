package dicoding.kade.sub5footballapp.favorite


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub5footballapp.adapter.FavoriteTeamAdapter
import dicoding.kade.sub5footballapp.detail.DetailTeamActivity
import dicoding.kade.sub5footballapp.model.TeamFavorite
import dicoding.kade.sub5footballapp.model.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.swipeRefreshLayout


class FavoriteTeamFragment : Fragment(), AnkoComponent<Context> {

    private var teamFavorites: MutableList<TeamFavorite> = mutableListOf()
    private lateinit var adapter: FavoriteTeamAdapter
    private lateinit var listTeam: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //TODO : setup RecyclerView Adapter & send Extras through Intent
        adapter = FavoriteTeamAdapter(teamFavorites){
            startActivity<DetailTeamActivity>(
                    "team_Id" to "${it.teamId}",
                    "team_name" to "${it.teamName}",
                    "team_badge" to "${it.teamBadge}",
                    "team_desc" to "${it.teamDescription}",
                    "team_stadium" to "${it.teamStadium}",
                    "team_year" to "${it.teamFormedYear}"
            )
        }

        listTeam.adapter = adapter
        showFavorite()
        swipeRefresh.onRefresh {
            teamFavorites.clear()
            showFavorite()
        }
    }

    private fun showFavorite(){
        context?.database?.use{
            swipeRefresh.isRefreshing = false
            val result = select(TeamFavorite.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<TeamFavorite>())
            teamFavorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    //TODO : ANKO-Layout for FavoriteTeamFragment
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            swipeRefresh = swipeRefreshLayout {
                id = R.id.fav_swipeRefreshTeamId
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        lparams(width = matchParent, height = wrapContent)
                        id = R.id.fav_recyclerViewTeamId
                        layoutManager = LinearLayoutManager(ctx)
                    }
                }
            }
        }

    }

    //TODO : Change immediately if there are changes in Favourite
    override fun onResume() {
        super.onResume()
        teamFavorites.clear()
        showFavorite()
    }

}
