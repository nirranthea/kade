package dicoding.kade.sub5footballapp.favorite


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub5footballapp.adapter.FavoriteScheduleAdapter
import dicoding.kade.sub5footballapp.detail.DetailMatchActivity
import dicoding.kade.sub5footballapp.model.ScheduleFavorite
import dicoding.kade.sub5footballapp.model.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.*


class FavoriteScheduleFragment : Fragment(), AnkoComponent<Context> {

    private var favorites: MutableList<ScheduleFavorite> = mutableListOf()
    private lateinit var adapter: FavoriteScheduleAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //TODO : setup RecyclerView Adapter & send Extras through Intent
        adapter = FavoriteScheduleAdapter(favorites) {
            startActivity<DetailMatchActivity>(
                    "teamHome" to "${it.teamHomeName}",
                    "teamAway" to "${it.teamAwayName}",
                    "scoreHome" to it.scoreHome,
                    "scoreAway" to it.scoreAway,
                    "date" to "${it.matchDate}",
                    "time" to "${it.matchTime}",
                    "teamHomeId" to "${it.teamHomeId}",
                    "teamAwayId" to "${it.teamAwayId}",
                    "mode" to "${it.mode}",
                    "eventId" to "${it.eventId}",
                    "goalsHome" to "${it.goalsHome}",
                    "goalsAway" to "${it.goalsAway}",
                    "redCardsHome" to "${it.redCardsHome}",
                    "redCardsAway" to "${it.redCardsAway}",
                    "yellowCardsHome" to "${it.yellowCardsHome}",
                    "yellowCardsAway" to "${it.yellowCardsAway}",
                    "shotsHome" to it.shotsHome,
                    "shotsAway" to it.shotsAway,
                    "goalKeeperHome" to "${it.goalKeeperHome}",
                    "goalKeeperAway" to "${it.goalKeeperAway}",
                    "defenseHome" to "${it.defenseHome}",
                    "defenseAway" to "${it.defenseAway}",
                    "midfieldHome" to "${it.midfieldHome}",
                    "midfieldAway" to "${it.midfieldAway}",
                    "forwardHome" to "${it.forwardHome}",
                    "forwardAway" to "${it.forwardAway}")
        }

        listEvent.adapter = adapter
        showFavorite()
        swipeRefresh.onRefresh {
            favorites.clear()
            showFavorite()
        }
    }

    private fun showFavorite() {
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(ScheduleFavorite.TABLE_FAVORITE_MATCH)
            val favorite = result.parseList(classParser<ScheduleFavorite>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    //TODO : ANKO-Layout for FavoriteScheduleFragment
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            swipeRefresh = swipeRefreshLayout {
                id = R.id.fav_swipeRefreshId
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listEvent = recyclerView {
                        lparams(width = matchParent, height = wrapContent)
                        id = R.id.fav_recyclerViewId
                        layoutManager = LinearLayoutManager(ctx)
                    }
                }
            }
        }

    }

    //TODO : Change immediately if there are changes in Favourite
    override fun onResume() {
        super.onResume()
        favorites.clear()
        showFavorite()
    }
}




