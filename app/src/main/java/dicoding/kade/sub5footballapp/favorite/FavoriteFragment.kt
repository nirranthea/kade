package dicoding.kade.sub5footballapp.favorite


import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub5footballapp.adapter.TabFavoriteAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.viewPager


class FavoriteFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d("Fragment","onCreateView ScheduleFragment")
        val view = FavoriteFragmentUI().createView(AnkoContext.create(ctx, this))
        val pager: ViewPager = view.findViewById(R.id.viewPagerFavoriteId)
        pager.adapter = TabFavoriteAdapter(childFragmentManager)

        val tab: TabLayout = view.findViewById(R.id.tabLayoutFavoriteId)
        tab.setupWithViewPager(pager)
        tab.tabGravity = TabLayout.GRAVITY_FILL

        return view
    }

    //TODO: ANKO-Layout for Favorite Fragment
    class FavoriteFragmentUI : AnkoComponent<FavoriteFragment>{
        private lateinit var tabFavorite: TabLayout
        private lateinit var pagerFavorite: ViewPager

        override fun createView(ui: AnkoContext<FavoriteFragment>) = with(ui) {
            verticalLayout {
                lparams(width = matchParent, height = wrapContent)

                tabFavorite = tabLayout {
                    lparams(width = matchParent, height = wrapContent)
                    id = R.id.tabLayoutFavoriteId
                    setTabTextColors(Color.BLACK, Color.WHITE)
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimary)
                }
                pagerFavorite = viewPager {
                    id = R.id.viewPagerFavoriteId
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }


}
