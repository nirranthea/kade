package dicoding.kade.sub5footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import dicoding.kade.sub5footballapp.favorite.FavoriteScheduleFragment
import dicoding.kade.sub5footballapp.favorite.FavoriteTeamFragment

class TabFavoriteAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    val title = arrayOf("Match", "Team")

    override fun getItem(position: Int): Fragment {
        when (position){
            0 -> return FavoriteScheduleFragment()
            1 -> return FavoriteTeamFragment()
        }
        return FavoriteScheduleFragment()
    }

    override fun getCount(): Int {
        return title.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }
}