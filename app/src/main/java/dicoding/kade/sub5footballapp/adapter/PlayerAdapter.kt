package dicoding.kade.sub5footballapp.adapter

import android.graphics.Color.RED
import android.graphics.Typeface.BOLD
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.*
import dicoding.kade.sub5footballapp.model.Player
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class PlayerAdapter(private val players: List<Player>, private val listener: (Player) -> Unit)
    : RecyclerView.Adapter<PlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        return PlayerViewHolder(PlayerUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = players.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(players[position], listener)
    }


    //TODO : ANKO-Layout for PlayerAdapter
    class PlayerUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
//                linearLayout {
                relativeLayout{
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(16)
//                    orientation = LinearLayout.HORIZONTAL

                    imageView {
                        id = R.id.player_profpic
                    }.lparams {
                        height = dip(50)
                        width = dip(50)
                        alignParentLeft()
                    }

                    textView {
                        id = R.id.player_name
                        textSize = 16f
                    }.lparams {
                        margin = dip(15)
                        width = wrapContent
                        rightOf(player_profpic)
                    }

                    textView {
                        id = R.id.player_position
                        textSize = 18f
                        textAppearance = BOLD
                        textColor = RED
                    }.lparams {
                        margin = dip(15)
                        width = wrapContent
                        alignParentRight()
                    }
                }
            }
        }
    }
}

//TODO: RecyclerView Holder for PlayerAdapter
class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val playerProfPic: ImageView = view.find(player_profpic)
    private val playerName: TextView = view.find(player_name)
    private val playerPosition: TextView = view.find(player_position)

    fun bindItem(player: Player, listener: (Player) -> Unit) {
        Picasso.get().load(player.playerProfilePicture).into(playerProfPic)
        playerName.text = player.playerName
        val positionCode: String = when (player.playerPosition){
            "Goalkeeper" -> "GK"
            "Midfielder" -> "MF"
            "Defender" -> "DF"
            "Forward" -> "FW"
            "Winger" -> "W"
            "Left Back" -> "LB"
            "Right Back" -> "RB"
            "Defensive Midfielder" -> "DM"
            else -> player.playerPosition.toString()
        }
        playerPosition.text = positionCode
        itemView.onClick { listener(player) }
    }

}
