package dicoding.kade.sub5footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import dicoding.kade.sub5footballapp.detail.DetailOverviewFragment
import dicoding.kade.sub5footballapp.detail.DetailPlayerFragment

class TabDetailTeamAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

    val title = arrayOf("Overview", "Players")

    override fun getItem(position: Int): Fragment {
        when(position){
            0 -> return DetailOverviewFragment()
            1 -> return DetailPlayerFragment()
        }
        return DetailOverviewFragment()
    }

    override fun getCount(): Int {
        return title.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }

}