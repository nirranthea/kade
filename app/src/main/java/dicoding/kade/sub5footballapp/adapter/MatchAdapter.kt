package dicoding.kade.sub5footballapp.adapter

import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_EDIT
import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.provider.CalendarContract
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.*
import dicoding.kade.sub5footballapp.model.Schedule
import dicoding.kade.sub5footballapp.utils.convertDateAndTime
import dicoding.kade.sub5footballapp.utils.convertDateOnly
import dicoding.kade.sub5footballapp.utils.toDate
import org.jetbrains.anko.*
import java.util.*

class MatchAdapter(private val schedules: List<Schedule>,
                   private val mode: String,
                   private val listener: (Schedule) -> Unit)
    : RecyclerView.Adapter<MatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(ScheduleUI().createView(AnkoContext.create(parent.context, parent)),mode, parent.context)
    }

    override fun getItemCount(): Int = schedules.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(schedules[position], listener)
    }

    //TODO: ANKO-layout for RecyclerView items
    class ScheduleUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.VERTICAL

                    textView {
                        id = R.id.schedule_date
                        textSize = 12f
                        textColor = Color.MAGENTA
                    }.lparams {
                        topMargin = dip(16)
                        bottomMargin = dip(8)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }

                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)
                        orientation = LinearLayout.HORIZONTAL
                        gravity = Gravity.CENTER

                        textView {
                            id = R.id.team_home
                            textSize = 14f
                            textColor = Color.BLUE
                            gravity = Gravity.END
                        }.lparams(width = dip(50), height = wrapContent, weight = 3f) {
                            margin = dip(8)

                        }
                        textView {
                            id = R.id.score_home
                            textSize = 20f
                            textColor = Color.BLACK
                            textAppearance = BOLD
                            gravity = Gravity.END
                        }.lparams(width = dip(10), height = wrapContent, weight = 1f) {
                            margin = dip(4)
                        }
                        textView {
                            id = R.id.vs
                            textSize = 12f
                            textColor = Color.BLACK
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams(width = wrapContent, height = wrapContent, weight = 1f) {
                            margin = dip(2)
                        }
                        textView {
                            id = R.id.score_away
                            textSize = 20f
                            textColor = Color.BLACK
                            textAppearance = BOLD
                            gravity = Gravity.START
                        }.lparams(width = dip(10), height = wrapContent, weight = 1f) {
                            margin = dip(4)
                        }
                        textView {
                            id = R.id.team_away
                            textSize = 14f
                            textColor = Color.RED
                            gravity = Gravity.START
                        }.lparams(width = dip(50), height = wrapContent, weight = 3f) {
                            margin = dip(8)
                        }
                        imageView {
                            id = R.id.add_to_alarm
                            imageResource = R.drawable.ic_alarm_add
                        }.lparams{
                            width = dip(25)
                            height = dip(25)
                            gravity = Gravity.END
                            rightMargin = dip(16)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        bottomMargin = dip(8)
                    }

                    view {
                        backgroundColor = Color.DKGRAY
                    }.lparams(width = matchParent, height = dip(0.5f)) {
                        topMargin = dip(16)
                    }
                }
            }
        }
    }
}

//TODO: RecyclerView Holder for MatchAdapter
class MatchViewHolder(view: View, mode: String, context: Context) : RecyclerView.ViewHolder(view) {

    private val matchTeamHome: TextView = view.find(team_home)
    private val matchTeamAway: TextView = view.find(team_away)
    private val matchScoreHome: TextView = view.find(score_home)
    private val matchScoreAway: TextView = view.find(score_away)
    private val matchScheduleDate: TextView = view.find(schedule_date)
    private val matchVs: TextView = view.find(vs)

    private val matchAlarm: ImageView = view.find(add_to_alarm)
    private val modeString: String = mode
    private val mContext = context

    fun bindItem(schedule: Schedule, listener: (Schedule) -> Unit) {
        matchTeamHome.text = schedule.teamHome
        matchTeamAway.text = schedule.teamAway
        if (schedule.scoreHome != null) {
            matchScoreHome.text = schedule.scoreHome.toString()
        } else {
            matchScoreHome.text = "?"
        }
        if (schedule.scoreAway != null) {
            matchScoreAway.text = schedule.scoreAway.toString()
        } else {
            matchScoreAway.text = "?"
        }
        matchVs.text = "vs"

        //TODO: show only Date for Search Result (because there are many different format for strTime)
        if (schedule.time != null && schedule.date != null) {
            if (modeString == "MatchSearch") {
                matchScheduleDate.text = convertDateOnly(schedule.date)
            } else{
                matchScheduleDate.text = convertDateAndTime(schedule.date + " " + schedule.time)
            }
        }
        itemView.setOnClickListener {
            listener(schedule)
        }

        //TODO : hide Alarm icon for Prev. Match Tab
        if (modeString == "prevMatch" || modeString == "MatchSearch"){
            matchAlarm.visibility = View.GONE
        } else{
            //TODO : insert Calendar Event when Alarm icon is clicked
            matchAlarm.setOnClickListener{
                val alarmIntent = Intent(ACTION_EDIT)
                alarmIntent.type = "vnd.android.cursor.item/event"
                alarmIntent.putExtra(CalendarContract.Events.TITLE,"${schedule.teamHome} vs ${schedule.teamAway}")
                val dateStart = toDate(schedule.date + " " + schedule.time)
                val dateEnd = Calendar.getInstance()
                dateEnd.time = dateStart
                dateEnd.add(Calendar.MINUTE,90)
                if (dateStart != null) {
                    alarmIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,dateStart.time)
                    alarmIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,dateEnd.time.time)
                    alarmIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY,false)
                } else{
                    alarmIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY,true)
                }
                alarmIntent.putExtra(CalendarContract.Events.DESCRIPTION,
                        "Next Match is ${schedule.teamHome} vs ${schedule.teamAway}")
                mContext.startActivity(alarmIntent)
            }
        }
    }
}
