package dicoding.kade.sub5footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import dicoding.kade.sub5footballapp.match.MatchFragment

class TabScheduleAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    val title = arrayOf("Prev. Match", "Next Match")

    override fun getCount(): Int {
        return title.size
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return MatchFragment.newInstance("prevMatch")
            1 -> return MatchFragment.newInstance("nextMatch")
        }
        return MatchFragment()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }

}