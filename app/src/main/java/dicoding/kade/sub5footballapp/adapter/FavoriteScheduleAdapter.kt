package dicoding.kade.sub5footballapp.adapter

import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.*
import dicoding.kade.sub5footballapp.model.ScheduleFavorite
import dicoding.kade.sub5footballapp.utils.convertDateAndTime
import dicoding.kade.sub5footballapp.utils.convertDateOnly
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class FavoriteScheduleAdapter(private val favorite: List<ScheduleFavorite>,
                              private val listener: (ScheduleFavorite) -> Unit)
    : RecyclerView.Adapter<FavoriteScheduleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteScheduleViewHolder {
        return FavoriteScheduleViewHolder(MatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: FavoriteScheduleViewHolder, position: Int) {
        holder.bindItem(favorite[position], listener)
    }

}

//TODO : ANKO-Layout for FavoriteScheduleAdapter
class MatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL

                textView {
                    id = R.id.fav_date
                    textSize = 12f
                    textColor = Color.MAGENTA
                }.lparams {
                    topMargin = dip(16)
                    bottomMargin = dip(8)
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER

                    textView {
                        id = R.id.fav_team_home
                        textSize = 14f
                        textColor = Color.BLUE
                        gravity = Gravity.END
                    }.lparams(width = dip(50), height = wrapContent, weight = 3f) {
                        margin = dip(8)

                    }
                    textView {
                        id = R.id.fav_score_home
                        textSize = 20f
                        textColor = Color.BLACK
                        textAppearance = Typeface.BOLD
                        gravity = Gravity.END
                    }.lparams(width = dip(10), height = wrapContent, weight = 1f) {
                        margin = dip(4)
                    }
                    textView {
                        id = R.id.vs
                        textSize = 12f
                        textColor = Color.BLACK
                        text = context.getString(R.string.text_vs)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams(width = wrapContent, height = wrapContent, weight = 1f) {
                        margin = dip(2)
                    }
                    textView {
                        id = R.id.fav_score_away
                        textSize = 20f
                        textColor = Color.BLACK
                        textAppearance = Typeface.BOLD
                        gravity = Gravity.START
                    }.lparams(width = dip(10), height = wrapContent, weight = 1f) {
                        margin = dip(4)
                    }
                    textView {
                        id = R.id.fav_team_away
                        textSize = 14f
                        textColor = Color.RED
                        gravity = Gravity.START
                    }.lparams(width = dip(50), height = wrapContent, weight = 3f) {
                        margin = dip(8)
                    }
                }.lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                }

                view {
                    backgroundColor = Color.DKGRAY
                }.lparams(width = matchParent, height = dip(0.5f)) {
                    topMargin = dip(16)
                }
            }
        }
    }

}

class FavoriteScheduleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val teamHomeName: TextView = view.find(fav_team_home)
    private val teamAwayName: TextView = view.find(fav_team_away)
    private val scoreHome: TextView = view.find(fav_score_home)
    private val scoreAway: TextView = view.find(fav_score_away)
    private val matchDate: TextView = view.find(fav_date)

    fun bindItem(favorite: ScheduleFavorite, listener: (ScheduleFavorite) -> Unit) {
        teamHomeName.text = favorite.teamHomeName
        teamAwayName.text = favorite.teamAwayName
        //TODO : prevMatch show the score, else nextMatch show "?"
        if (favorite.mode == "prevMatch" || favorite.mode == "MatchSearch"){
            scoreHome.text = favorite.scoreHome.toString()
            scoreAway.text = favorite.scoreAway.toString()
        } else{
            scoreHome.text = "?"
            scoreAway.text = "?"
        }

        //TODO: show only Date for Search Result (because there are many different format for strTime)
        if (favorite.matchTime != null && favorite.matchDate != null) {
            if (favorite.mode == "MatchSearch") {
                matchDate.text = convertDateOnly(favorite.matchDate)
            } else{
                matchDate.text = convertDateAndTime(favorite.matchDate + " " + favorite.matchTime)
            }
        }
        itemView.onClick { listener(favorite) }
    }
}
