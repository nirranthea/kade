package dicoding.kade.sub5footballapp.utils

import android.view.View
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun convertDateAndTime(dateAndTime: String?):String?{
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX", Locale.ENGLISH)
    var myDate: Date? = null
    try {
        myDate = dateFormat.parse(dateAndTime)
    } catch (e: ParseException){
        e.printStackTrace()
    }
    val timeFormat = SimpleDateFormat("EEE, dd MMM yyyy HH:mm", Locale.getDefault())

    return timeFormat.format(myDate)
}

fun convertDateOnly(date: String?):String?{
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    var myDate: Date? = null
    try {
        myDate = dateFormat.parse(date)
    } catch (e: ParseException){
        e.printStackTrace()
    }
    val timeFormat = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())

    return timeFormat.format(myDate)
}

fun toDate(dateAndTime: String?): Date?{
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX", Locale.ENGLISH)
    return dateFormat.parse(dateAndTime)
}

fun verifyStringNull(input: String?):String{
    return if (input.equals("null")){
        "-"
    } else{
        splitStringIntoMultiLine(input)
    }
}

fun splitStringIntoMultiLine(input: String?):String{
    val sb = StringBuilder("")
    val lines = input?.split(";")
    for(number in 0..lines!!.size-2){
        sb.append(lines[number])
        sb.append("\n")
    }
    sb.append(lines[lines.size-1])
    return sb.toString()
}