package dicoding.kade.sub5footballapp.detail

import android.util.Log
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.Player
import dicoding.kade.sub5footballapp.model.PlayerResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPlayerPresenter(private val view: DetailPlayerView,
                            private val apiClient: ApiClient) {

    fun getPlayerList(teamId: String){
        view.showLoading()

        val cek: Call<PlayerResponse> = apiClient.get().getPlayer(teamId)
        cek.enqueue(object : Callback<PlayerResponse> {
            override fun onResponse(call: Call<PlayerResponse>, response: Response<PlayerResponse>) {
                Log.d("Retrofit", "success")
                if (response.isSuccessful && response.body() != null) {
                    Log.d("Retrofit2", response.body().toString())
                    val listResponse: List<Player>? = response.body()!!.players
                    Log.d("Retrofit3", listResponse.toString())
                    view.hideLoading()
                    if (listResponse != null) view.showPlayerList(listResponse)
                }
            }

            override fun onFailure(call: Call<PlayerResponse>?, t: Throwable?) {
                Log.d("Retrofit", "failure : " + t?.message)
                view.hideLoading()
            }
        })
    }
}