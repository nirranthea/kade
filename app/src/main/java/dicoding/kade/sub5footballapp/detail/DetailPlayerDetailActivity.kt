package dicoding.kade.sub5footballapp.detail

import android.graphics.Color
import android.graphics.Color.*
import android.graphics.Typeface.BOLD
import android.graphics.Typeface.ITALIC
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import dicoding.kade.sub2footballmatchschedule.R
import org.jetbrains.anko.*

class DetailPlayerDetailActivity : AppCompatActivity() {

    private lateinit var detailPlayerUI: DetailPlayerDetailActivityUI
    private var detPlayerName: String = ""
    private var detPlayerPosition: String? = ""
    private var detPlayerDesc: String? = ""
    private var detPlayerNationality: String? = ""
    private var detPlayerHeight: String? = ""
    private var detPlayerWeight: String? = ""
    private var detPlayerThumb: String? = ""
    private var detPlayerFanart: String? = ""
    private var imagePlayer: ImageView? = null
    private lateinit var tvPlayerHeight: TextView
    private lateinit var tvPlayerWeight: TextView
    private lateinit var tvPlayerPosition: TextView
    private lateinit var tvPlayerNationality: TextView
    private lateinit var tvPlayerDesc: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailPlayerUI = DetailPlayerDetailActivityUI()
        detailPlayerUI.setContentView(this)


        //TODO: get Intent's Extra
        val playerIntent = intent
        detPlayerName = playerIntent.getStringExtra("player_name")
        detPlayerPosition = playerIntent.getStringExtra("player_position")
        detPlayerDesc = playerIntent.getStringExtra("player_desc")
        detPlayerNationality = playerIntent.getStringExtra("player_nationality")
        detPlayerHeight = playerIntent.getStringExtra("player_height")
        detPlayerWeight = playerIntent.getStringExtra("player_weight")
        detPlayerThumb = playerIntent.getStringExtra("player_thumbnail")
        detPlayerFanart = playerIntent.getStringExtra("player_fanart")

        //TODO: set Action Bar's title & back Button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = detPlayerName

        imagePlayer = detailPlayerUI.playerThumb
        if (detPlayerFanart != null) {
            Picasso.get().load(detPlayerFanart).fit().into(imagePlayer)
        } else {
            Picasso.get().load(detPlayerThumb).fit().into(imagePlayer)
        }

        tvPlayerHeight = detailPlayerUI.playerHeight
        tvPlayerHeight.text = detPlayerHeight
        tvPlayerWeight = detailPlayerUI.playerWeight
        tvPlayerWeight.text = detPlayerWeight
        tvPlayerPosition = detailPlayerUI.playerPosition
        tvPlayerPosition.text = detPlayerPosition
        tvPlayerNationality = detailPlayerUI.playerNationality
        tvPlayerNationality.text = detPlayerNationality
        tvPlayerDesc = detailPlayerUI.playerDesc
        tvPlayerDesc.text = detPlayerDesc
    }

    //TODO: Item in Option Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //TODO: ANKO-Layout for DetailPlayerDetailActivity
    class DetailPlayerDetailActivityUI : AnkoComponent<DetailPlayerDetailActivity> {
        lateinit var playerThumb: ImageView
        lateinit var playerHeight: TextView
        lateinit var playerWeight: TextView
        lateinit var playerPosition: TextView
        lateinit var playerNationality: TextView
        lateinit var playerDesc: TextView

        override fun createView(ui: AnkoContext<DetailPlayerDetailActivity>): View = with(ui) {
            scrollView {
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.VERTICAL

                    playerThumb = imageView {
                    }.lparams {
                        width = matchParent
                        height = dip(200)
                        bottomMargin = dip(8)
                    }



                    linearLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(16)
                            bottomMargin = dip(16)
                        }
                        orientation = LinearLayout.HORIZONTAL
                        gravity = Gravity.CENTER

                        //Weight
                        relativeLayout {
                            gravity = Gravity.CENTER_HORIZONTAL

                            textView {
                                id = R.id.weight_text
                                textColor = BLACK
                                text = context.getString(R.string.player_weight)
                            }.lparams {
                                centerHorizontally()
                            }
                            playerWeight = textView {
                                textSize = 24f
                                textAppearance = BOLD
                                textColor = BLUE
                            }.lparams {
                                below(R.id.weight_text)
                                centerHorizontally()
                            }
                        }.lparams(width = matchParent, height = wrapContent, weight = 1f) {
                            rightMargin = dip(8)
                        }
                        //Height
                        relativeLayout {
                            gravity = Gravity.CENTER_HORIZONTAL

                            textView {
                                id = R.id.height_text
                                textColor = BLACK
                                text = context.getString(R.string.player_height)
                            }.lparams {
                                centerHorizontally()
                            }
                            playerHeight = textView {
                                textSize = 24f
                                textAppearance = BOLD
                                textColor = BLUE
                            }.lparams {
                                below(R.id.height_text)
                                centerHorizontally()
                            }
                        }.lparams(width = matchParent, height = wrapContent, weight = 1f) {
                            leftMargin = dip(8)
                        }
                    }

                    playerPosition = textView {
                        textSize = 20f
                        textAppearance = BOLD
                        textColor = RED
                    }.lparams {
                        bottomMargin = dip(8)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }

                    playerNationality = textView {
                        textSize = 16f
                        textAppearance = ITALIC
                        textColor = MAGENTA
                    }.lparams {
                        bottomMargin = dip(8)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }

                    view {
                        backgroundColor = Color.DKGRAY
                    }.lparams(width = matchParent, height = dip(0.5f)) {
                        bottomMargin = dip(8)
                    }

                    playerDesc = textView {
                        textSize = 14f
                    }.lparams(width = matchParent, height = wrapContent) {
                        leftMargin = dip(8)
                        rightMargin = dip(8)
                    }

                }
            }
        }


    }
}
