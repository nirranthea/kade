package dicoding.kade.sub5footballapp.detail

import android.database.sqlite.SQLiteConstraintException
import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.add_to_favorite
import dicoding.kade.sub2footballmatchschedule.R.menu.menu_details
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.*
import dicoding.kade.sub5footballapp.utils.convertDateAndTime
import dicoding.kade.sub5footballapp.utils.convertDateOnly
import dicoding.kade.sub5footballapp.utils.verifyStringNull
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.delete

class DetailMatchActivity : AppCompatActivity() {

    private lateinit var detailUI: DetailActivityUI
    private lateinit var matchPresenter: DetailMatchPresenter
    private lateinit var matchPresenter2: DetailMatchPresenter
    private var detailTeamHome: String = ""
    private var detailTeamAway: String = ""
    private var detailScoreHome: Int = 0
    private var detailScoreAway: Int = 0
    private var detailDate: String = ""
    private var detailTime: String = ""
    private var detailEventId: String = ""
    private var detailTeamHomeId: String = ""
    private var detailTeamAwayId: String = ""
    private var detailTeamStr: String? = ""
    private var detailGoalsHome: String? = ""
    private var detailGoalsAway: String? = ""
    private var detailShotsHome: Int = 0
    private var detailShotsAway: Int = 0
    private var detailRedCardsHome: String? = ""
    private var detailRedCardsAway: String? = ""
    private var detailYellowCardsHome: String? = ""
    private var detailYellowCardsAway: String? = ""
    private var detailGoalKeeperHome: String? = ""
    private var detailGoalKeeperAway: String? = ""
    private var detailDefenseHome: String? = ""
    private var detailDefenseAway: String? = ""
    private var detailMidfieldHome: String? = ""
    private var detailMidfieldAway: String? = ""
    private var detailForwardHome: String? = ""
    private var detailForwardAway: String? = ""
    private val teams: MutableList<ScheduleDetail> = mutableListOf()
    private var badgeHome: ImageView? = null
    private var badgeAway: ImageView? = null
    private var detailMode: String? = ""
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var schedules: Schedule


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailUI = DetailActivityUI()
        detailUI.setContentView(this)

        //TODO: set Action Bar's title & back Button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Match Detail"

        //TODO: get intent's Extra
        val intent = intent
        detailTeamHome = intent.getStringExtra("teamHome")
        detailTeamAway = intent.getStringExtra("teamAway")
        detailScoreHome = intent.getIntExtra("scoreHome", 0)
        detailScoreAway = intent.getIntExtra("scoreAway", 0)
        detailDate = intent.getStringExtra("date")
        detailTime = intent.getStringExtra("time")
        detailEventId = intent.getStringExtra("eventId")
        detailTeamHomeId = intent.getStringExtra("teamHomeId")
        detailTeamAwayId = intent.getStringExtra("teamAwayId")
        detailMode = intent.getStringExtra("mode")
        detailGoalsHome = intent.getStringExtra("goalsHome")
        detailGoalsAway = intent.getStringExtra("goalsAway")
        detailShotsHome = intent.getIntExtra("shotsHome", 0)
        detailShotsAway = intent.getIntExtra("shotsAway", 0)
        detailRedCardsHome = intent.getStringExtra("redCardsHome")
        detailRedCardsAway = intent.getStringExtra("redCardsAway")
        detailYellowCardsHome = intent.getStringExtra("yellowCardsHome")
        detailYellowCardsAway = intent.getStringExtra("yellowCardsAway")
        detailGoalKeeperHome = intent.getStringExtra("goalKeeperHome")
        detailGoalKeeperAway = intent.getStringExtra("goalKeeperAway")
        detailDefenseHome = intent.getStringExtra("defenseHome")
        detailDefenseAway = intent.getStringExtra("defenseAway")
        detailMidfieldHome = intent.getStringExtra("midfieldHome")
        detailMidfieldAway = intent.getStringExtra("midfieldAway")
        detailForwardHome = intent.getStringExtra("forwardHome")
        detailForwardAway = intent.getStringExtra("forwardAway")

        //TODO: set intent's Extra into Components
        if (detailMode == "MatchSearch"){
            detailUI.detDate.text = convertDateOnly(detailDate)
        } else{
            detailUI.detDate.text = convertDateAndTime("$detailDate $detailTime")
        }
        detailUI.detTeamHomeName.text = detailTeamHome
        detailUI.detTeamAwayName.text = detailTeamAway
        if (detailMode == "prevMatch" || detailMode == "MatchSearch") {
            detailUI.detScoreHome.text = detailScoreHome.toString()
            detailUI.detScoreAway.text = detailScoreAway.toString()
            detailUI.detShotsHome.text = detailShotsHome.toString()
            detailUI.detShotsAway.text = detailShotsAway.toString()
        } else {
            detailUI.detScoreHome.text = "?"
            detailUI.detScoreAway.text = "?"
            detailUI.detShotsHome.text = "-"
            detailUI.detShotsAway.text = "-"
        }
        detailUI.detGoalHome.text = verifyStringNull(detailGoalsHome)
        detailUI.detGoalAway.text = verifyStringNull(detailGoalsAway)
        detailUI.detRedCardsHome.text = verifyStringNull(detailRedCardsHome)
        detailUI.detRedCardsAway.text = verifyStringNull(detailRedCardsAway)
        detailUI.detYellowCardsHome.text = verifyStringNull(detailYellowCardsHome)
        detailUI.detYellowCardsAway.text = verifyStringNull(detailYellowCardsAway)
        detailUI.detGoalkeeperHome.text = verifyStringNull(detailGoalKeeperHome)
        detailUI.detGoalkeeperAway.text = verifyStringNull(detailGoalKeeperAway)
        detailUI.detDefenseHome.text = verifyStringNull(detailDefenseHome)
        detailUI.detDefenseAway.text = verifyStringNull(detailDefenseAway)
        detailUI.detMidfieldHome.text = verifyStringNull(detailMidfieldHome)
        detailUI.detMidfieldAway.text = verifyStringNull(detailMidfieldAway)
        detailUI.detForwardHome.text = verifyStringNull(detailForwardHome)
        detailUI.detForwardAway.text = verifyStringNull(detailForwardAway)

        //TODO : initialize schedules object for Favorite
        schedules = Schedule(
                detailTeamHome,
                detailTeamAway,
                detailScoreHome,
                detailScoreAway,
                detailDate,
                detailTime,
                detailTeamHomeId,
                detailTeamAwayId,
                detailEventId,
                detailGoalsHome,
                detailGoalsAway,
                detailShotsHome,
                detailShotsAway,
                detailRedCardsHome,
                detailRedCardsAway,
                detailYellowCardsHome,
                detailYellowCardsAway,
                detailGoalKeeperHome,
                detailGoalKeeperAway,
                detailDefenseHome,
                detailDefenseAway,
                detailMidfieldHome,
                detailMidfieldAway,
                detailForwardHome,
                detailForwardAway)

        //TODO: get Team Details & Set Logo
        val request = ApiClient()
        matchPresenter = DetailMatchPresenter(this, request)
        matchPresenter2 = DetailMatchPresenter(this, request)

        badgeHome = detailUI.detTeamHomeLogo
        matchPresenter.getTeamDetail(detailTeamHomeId, "home")

        badgeAway = detailUI.detTeamAwayLogo
        matchPresenter2.getTeamDetail(detailTeamAwayId, "away")

        //TODO: Call favoriteState
        favoriteState()

    }

    //TODO : set Team Logo
    fun getDetail(data: List<ScheduleDetail>, position: String) {
        teams.clear()
        teams.addAll(data)
        detailTeamStr = teams[0].teamLogo
        Log.d("Retrofit4", detailTeamStr)
        when (position) {
            "home" -> Picasso.get().load(detailTeamStr).into(badgeHome)
            "away" -> Picasso.get().load(detailTeamStr).into(badgeAway)
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(ScheduleFavorite.TABLE_FAVORITE_MATCH,
                        ScheduleFavorite.EVENT_ID to schedules.eventId,
                        ScheduleFavorite.TEAM_HOME_ID to schedules.teamHomeId,
                        ScheduleFavorite.TEAM_AWAY_ID to schedules.teamAwayId,
                        ScheduleFavorite.TEAM_HOME_NAME to schedules.teamHome,
                        ScheduleFavorite.TEAM_AWAY_NAME to schedules.teamAway,
                        ScheduleFavorite.SCORE_HOME to schedules.scoreHome.toString(),
                        ScheduleFavorite.SCORE_AWAY to schedules.scoreAway.toString(),
                        ScheduleFavorite.MATCH_DATE to schedules.date,
                        ScheduleFavorite.MATCH_TIME to schedules.time,
                        ScheduleFavorite.MODE to detailMode,
                        ScheduleFavorite.GOALS_HOME to schedules.goalsHome,
                        ScheduleFavorite.GOALS_AWAY to schedules.goalsAway,
                        ScheduleFavorite.SHOTS_HOME to schedules.shotsHome.toString(),
                        ScheduleFavorite.SHOTS_AWAY to schedules.shotsAway.toString(),
                        ScheduleFavorite.REDCARDS_HOME to schedules.redCardsHome,
                        ScheduleFavorite.REDCARDS_AWAY to schedules.redCardsAway,
                        ScheduleFavorite.YELLOWCARDS_HOME to schedules.yellowCardsHome,
                        ScheduleFavorite.YELLOWCARDS_AWAY to schedules.yellowCardsAway,
                        ScheduleFavorite.GOALKEEPER_HOME to schedules.goalKeeperHome,
                        ScheduleFavorite.GOALKEEPER_AWAY to schedules.goalKeeperAway,
                        ScheduleFavorite.DEFENSE_HOME to schedules.defenseHome,
                        ScheduleFavorite.DEFENSE_AWAY to schedules.defenseAway,
                        ScheduleFavorite.MIDFIELD_HOME to schedules.midfieldHome,
                        ScheduleFavorite.MIDFIELD_AWAY to schedules.midfieldAway,
                        ScheduleFavorite.FORWARD_HOME to schedules.forwardHome,
                        ScheduleFavorite.FORWARD_AWAY to schedules.forwardAway)
            }
            Toast.makeText(this, "Added to Favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(ScheduleFavorite.TABLE_FAVORITE_MATCH, "(EVENT_ID = {eventId})",
                        "eventId" to detailEventId)
            }
            Toast.makeText(this, "Removed from Favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(ScheduleFavorite.TABLE_FAVORITE_MATCH)
                    .whereArgs("(EVENT_ID = {eventId})",
                            "eventId" to detailEventId)
            val favorite = result.parseList(classParser<ScheduleFavorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    //TODO : change for Favorite Icon
    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        } else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    //TODO : inflate menu_details from xml
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(menu_details, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    //TODO: Item in Option Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //TODO: ANKO-layout for DetailMatchActivity
    class DetailActivityUI : AnkoComponent<DetailMatchActivity> {
        lateinit var detTeamHomeLogo: ImageView
        lateinit var detTeamAwayLogo: ImageView
        lateinit var detTeamHomeName: TextView
        lateinit var detTeamAwayName: TextView
        lateinit var detScoreHome: TextView
        lateinit var detScoreAway: TextView
        lateinit var detDate: TextView
        lateinit var detGoalHome: TextView
        lateinit var detGoalAway: TextView
        lateinit var detShotsHome: TextView
        lateinit var detShotsAway: TextView
        lateinit var detRedCardsHome: TextView
        lateinit var detRedCardsAway: TextView
        lateinit var detYellowCardsHome: TextView
        lateinit var detYellowCardsAway: TextView
        lateinit var detGoalkeeperHome: TextView
        lateinit var detGoalkeeperAway: TextView
        lateinit var detDefenseHome: TextView
        lateinit var detDefenseAway: TextView
        lateinit var detMidfieldHome: TextView
        lateinit var detMidfieldAway: TextView
        lateinit var detForwardHome: TextView
        lateinit var detForwardAway: TextView

        override fun createView(ui: AnkoContext<DetailMatchActivity>) = with(ui) {

            linearLayout {
                lparams(width = matchParent, height = wrapContent) {
                    topMargin = dip(16)
                }
                orientation = LinearLayout.VERTICAL

                detDate = textView {
                    id = R.id.detail_date
                    textSize = 12f
                    textColor = Color.MAGENTA
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        topMargin = dip(16)
                    }
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER

                    //Team Home
                    relativeLayout {
                        lparams(width = dip(100), height = wrapContent) {
                            rightMargin = dip(16)
                        }
                        gravity = Gravity.START
                        detTeamHomeLogo = imageView {
                            id = R.id.detail_team_home_logo
                        }.lparams {
                            height = dip(100)
                            width = dip(100)
                            bottomMargin = dip(8)
                            centerHorizontally()
                        }
                        detTeamHomeName = textView {
                            id = R.id.detail_team_home_name
                            textSize = 16f
                            textColor = Color.BLUE
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams {
                            below(R.id.detail_team_home_logo)
                            centerHorizontally()
                        }
                    }

                    //Score Home
                    detScoreHome = textView {
                        id = R.id.detail_score_home
                        textSize = 20f
                        textAppearance = BOLD
                        textColor = Color.BLACK
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams {
                        margin = dip(8)
                    }

                    //vs
                    textView {
                        id = R.id.detail_vs
                        textSize = 14f
                        text = context.getString(R.string.text_vs)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams {
                        margin = dip(2)
                    }

                    //Score Away
                    detScoreAway = textView {
                        id = R.id.detail_score_away
                        textSize = 20f
                        textAppearance = BOLD
                        textColor = Color.BLACK
                        gravity = Gravity.CENTER_HORIZONTAL
                    }.lparams {
                        margin = dip(8)
                    }

                    //Team Away
                    relativeLayout {
                        lparams(width = dip(100), height = wrapContent) {
                            leftMargin = dip(16)
                        }
                        gravity = Gravity.END
                        detTeamAwayLogo = imageView {
                            id = R.id.detail_team_away_logo
                        }.lparams {
                            height = dip(100)
                            width = dip(100)
                            centerHorizontally()
                            bottomMargin = dip(8)
                        }
                        detTeamAwayName = textView {
                            id = R.id.detail_team_away_name
                            textSize = 16f
                            textColor = Color.BLUE
                            gravity = Gravity.CENTER_HORIZONTAL
                        }.lparams {
                            below(R.id.detail_team_away_logo)
                            centerHorizontally()
                        }
                    }
                }

                view {
                    backgroundColor = Color.DKGRAY
                }.lparams(width = matchParent, height = dip(0.5f)) {
                    topMargin = dip(16)
                }

                scrollView {
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent) {
                            topMargin = dip(16)
                            bottomMargin = dip(16)
                        }
                        orientation = LinearLayout.VERTICAL

                        // Goals Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_goals)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detGoalHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detGoalAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Shots Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_shots)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detShotsHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detShotsAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Red Cards Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_redcards)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detRedCardsHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detRedCardsAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Yellow Cards Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_yellowcards)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detYellowCardsHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detYellowCardsAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Goal Keeper Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_goalkeeper)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detGoalkeeperHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detGoalkeeperAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Defense Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_defense)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detDefenseHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detDefenseAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Midfield Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_midfield)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detMidfieldHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detMidfieldAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }

                        // Forward Detail
                        textView {
                            textSize = 12f
                            textColor = Color.MAGENTA
                            gravity = Gravity.CENTER_HORIZONTAL
                            text = context.getString(R.string.text_forward)
                        }
                        linearLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(4)
                                bottomMargin = dip(16)
                            }
                            orientation = LinearLayout.HORIZONTAL
                            gravity = Gravity.CENTER

                            detForwardHome = textView {
                                gravity = Gravity.END
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                rightMargin = dip(16)
                                gravity = Gravity.TOP
                            }

                            detForwardAway = textView {
                                gravity = Gravity.START
                            }.lparams(width = 0, height = wrapContent, weight = 1f) {
                                leftMargin = dip(16)
                                gravity = Gravity.TOP
                            }
                        }
                    }

                }
            }
        }
    }

}
