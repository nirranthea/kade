package dicoding.kade.sub5footballapp.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.team_overview
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.ctx

class DetailOverviewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = DetailOverviewFragmentUI().createView(AnkoContext.create(ctx, this))
        val teamDesc = activity?.intent?.getStringExtra("team_desc")
        val teamOverview: TextView = view.find(team_overview)
        teamOverview.text = teamDesc
        return view
    }

    class DetailOverviewFragmentUI : AnkoComponent<DetailOverviewFragment> {
        lateinit var description: TextView

        override fun createView(ui: AnkoContext<DetailOverviewFragment>): View = with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL

                scrollView {
                    description = textView {
                        id = R.id.team_overview
                        textSize = 14f
                    }.lparams{
                        leftMargin = dip (16)
                        rightMargin = dip (16)
                    }
                }
            }
        }

    }


}
