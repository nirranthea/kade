package dicoding.kade.sub5footballapp.detail

import android.util.Log
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.ScheduleDetail
import dicoding.kade.sub5footballapp.model.ScheduleDetailResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class   DetailMatchPresenter(private val detailMatchActivity: DetailMatchActivity,
                           private val apiClient: ApiClient) {

    fun getTeamDetail(teamId: String, position: String) {
        val detail: Call<ScheduleDetailResponse> = apiClient.get().getTeamDetail(teamId)
        detail.enqueue(object : Callback<ScheduleDetailResponse> {
            override fun onResponse(call: Call<ScheduleDetailResponse>, response: Response<ScheduleDetailResponse>) {
//                Log.d("Retrofit", "get Detail Success")
                if (response.isSuccessful && response.body() != null) {
//                    Log.d("Retrofit2", response.body().toString())
                    val listDetail: List<ScheduleDetail> = response.body()!!.scheduledetails
//                    Log.d("Retrofit3", listDetail.toString())
                    detailMatchActivity.getDetail(listDetail, position)
                }
            }

            override fun onFailure(call: Call<ScheduleDetailResponse>, t: Throwable) {
                Log.d("Retrofit", "get Detail failure : " + t.message)
            }
        })
    }

}