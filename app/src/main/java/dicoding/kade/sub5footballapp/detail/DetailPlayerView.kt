package dicoding.kade.sub5footballapp.detail

import dicoding.kade.sub5footballapp.model.Player

interface DetailPlayerView {

    fun showLoading()
    fun hideLoading()
    fun showPlayerList(data: List<Player>)

}