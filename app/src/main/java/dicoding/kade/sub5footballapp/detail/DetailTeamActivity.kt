package dicoding.kade.sub5footballapp.detail

import android.database.sqlite.SQLiteConstraintException
import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.add_to_favorite
import dicoding.kade.sub2footballmatchschedule.R.menu.menu_details
import dicoding.kade.sub5footballapp.adapter.TabDetailTeamAdapter
import dicoding.kade.sub5footballapp.model.Team
import dicoding.kade.sub5footballapp.model.TeamFavorite
import dicoding.kade.sub5footballapp.model.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.viewPager

class DetailTeamActivity : AppCompatActivity() {

    private lateinit var detailTeamUI : DetailTeamActivityUI
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var detailTeamId = ""
    private var detailTeamName = ""
    private var detailTeamBadge = ""
    private var detailTeamStadium = ""
    private var detailTeamYear = ""
    private var detailTeamDesc = ""
    private lateinit var teams: Team

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailTeamUI = DetailTeamActivityUI()
        detailTeamUI.setContentView(this)

        //TODO: set Action Bar's title & back Button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Team Detail"

        //TODO: Set TabLayout & ViewPager
        val tab:TabLayout = detailTeamUI.tabTeam
        val pager:ViewPager = detailTeamUI.pagerTeam
        pager.adapter = TabDetailTeamAdapter(supportFragmentManager)
        tab.setupWithViewPager(pager)
        tab.tabGravity = TabLayout.GRAVITY_FILL

        //TODO: get intent's Extra
        val intent = intent
        detailTeamId = intent.getStringExtra("team_Id")
        detailTeamName = intent.getStringExtra("team_name")
        detailTeamStadium = intent.getStringExtra("team_stadium")
        detailTeamYear = intent.getStringExtra("team_year")
        detailTeamBadge = intent.getStringExtra("team_badge")
        detailTeamDesc = intent.getStringExtra("team_desc")

        //TODO: set intent's Extra into Components
        Picasso.get().load(detailTeamBadge).into(detailTeamUI.detTeamLogo)
        detailTeamUI.detTeamName.text = detailTeamName
        detailTeamUI.detTeamYear.text = detailTeamYear
        detailTeamUI.detTeamStadium.text = detailTeamStadium

        //TODO : initialize team object for Favorite
        teams = Team(detailTeamId,
                detailTeamName,
                detailTeamBadge,
                detailTeamYear,
                detailTeamStadium,
                detailTeamDesc)

        //TODO: Call favoriteState
        favoriteState()

    }

    private fun addToFavorite(){
        try {
            database.use {
                insert(TeamFavorite.TABLE_FAVORITE_TEAM,
                       TeamFavorite.TEAM_ID to teams.teamId,
                        TeamFavorite.TEAM_NAME to teams.teamName,
                        TeamFavorite.TEAM_BADGE to teams.teamBadge,
                        TeamFavorite.TEAM_FORMEDYEAR to teams.teamFormedYear,
                        TeamFavorite.TEAM_STADIUM to teams.teamStadium,
                        TeamFavorite.TEAM_DESCRIPTION to teams.teamDescription)
            }
            Toast.makeText(this, "Added to Favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException){
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }

    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(TeamFavorite.TABLE_FAVORITE_TEAM, "(TEAM_ID = {teamId})",
                        "teamId" to detailTeamId)
            }
            Toast.makeText(this, "Removed from Favorite", Toast.LENGTH_SHORT).show()
        } catch (e: SQLiteConstraintException) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(TeamFavorite.TABLE_FAVORITE_TEAM)
                    .whereArgs("(TEAM_ID = {teamId})",
                            "teamId" to detailTeamId)
            val favorite = result.parseList(classParser<TeamFavorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    //TODO : change for Favorite Icon
    private fun setFavorite() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        } else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    //TODO : inflate menu_details from xml
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(menu_details, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    //TODO: Item in Option Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //TODO : ANKO-Layout for DetailTeamActivity
    class DetailTeamActivityUI: AnkoComponent<DetailTeamActivity>{
        lateinit var tabTeam : TabLayout
        lateinit var pagerTeam : ViewPager
        lateinit var detTeamLogo : ImageView
        lateinit var detTeamName : TextView
        lateinit var detTeamYear : TextView
        lateinit var detTeamStadium : TextView

        override fun createView(ui: AnkoContext<DetailTeamActivity>) =  with(ui) {
            verticalLayout {
                lparams(width = matchParent, height = wrapContent)

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    gravity = Gravity.CENTER
                    backgroundColor = ContextCompat.getColor(context,R.color.colorPrimaryDark)

                    detTeamLogo = imageView {
                    }.lparams{
                        height = dip(100)
                        width = dip(100)
                        topMargin = dip(16)
                        bottomMargin = dip(8)
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    detTeamName = textView {
                        textSize = 24f
                        textColor = Color.WHITE
                        textAppearance = BOLD
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    detTeamYear = textView {
                        textSize = 20f
                        textColor = Color.WHITE
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                    detTeamStadium = textView {
                        textSize = 16f
                        textColor = Color.WHITE
                        gravity = Gravity.CENTER_HORIZONTAL
                    }
                }

                tabTeam = tabLayout{
                    lparams(width = matchParent, height = wrapContent)
                    id = R.id.tabTeamId
                    setTabTextColors(Color.BLACK, Color.WHITE)
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimaryDark)
                }
                pagerTeam = viewPager {
                    id = R.id.viewPagerTeamId
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }

}
