package dicoding.kade.sub5footballapp.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub5footballapp.adapter.PlayerAdapter
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.Player
import dicoding.kade.sub5footballapp.utils.invisible
import dicoding.kade.sub5footballapp.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class DetailPlayerFragment : Fragment(), DetailPlayerView {

    private val players: MutableList<Player> = mutableListOf()
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var listAdapter: PlayerAdapter
    private lateinit var presenter: DetailPlayerPresenter
    private var teamID: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = DetailPlayerFragmentUI().createView(AnkoContext.create(ctx, this))

        listAdapter = PlayerAdapter(players){
            startActivity<DetailPlayerDetailActivity>(
                    "player_name" to "${it.playerName}",
                    "player_position" to "${it.playerPosition}",
                    "player_nationality" to "${it.playerNationality}",
                    "player_height" to "${it.playerHeight}",
                    "player_weight" to "${it.playerWeight}",
                    "player_thumbnail" to "${it.playerThumbnail}",
                    "player_fanart" to "${it.playerFanart}",
                    "player_desc" to "${it.playerDescription}"
            )
        }

        //TODO: initialize RecyclerView in ANKO fragment
        val rv: RecyclerView = view.findViewById(R.id.recyclerViewPlayerId)
        rv.adapter = listAdapter

        //TODO : initialize ProgressBar in ANKO fragment
        progressBar = view.findViewById(R.id.progressBarPlayerId)

        //TODO: initialize Presenter
        val request = ApiClient()
        val intent = activity?.intent
        teamID = intent!!.getStringExtra("team_Id")
        presenter = DetailPlayerPresenter(this, request)
        presenter.getPlayerList(teamID)

        //TODO: swipeRefresh behaviour
        swipeRefresh = view.findViewById(R.id.swipeRefreshPlayerId)
        swipeRefresh.onRefresh {
            presenter.getPlayerList(teamID)
        }

        return view
    }


    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showPlayerList(data: List<Player>) {
        swipeRefresh.isRefreshing = false
        players.clear()
        players.addAll(data)
        listAdapter.notifyDataSetChanged()
    }

    //TODO : ANKO-Layout for DetailPlayerFragment
    class DetailPlayerFragmentUI : AnkoComponent<DetailPlayerFragment>{
        override fun createView(ui: AnkoContext<DetailPlayerFragment>): View = with(ui){
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL

                swipeRefreshLayout {
                    id = R.id.swipeRefreshPlayerId
                    setColorSchemeResources(R.color.colorAccent,
                            android.R.color.holo_green_light,
                            android.R.color.holo_orange_light,
                            android.R.color.holo_red_light)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        recyclerView {
                            lparams(width = matchParent, height = wrapContent)
                            id = R.id.recyclerViewPlayerId
                            layoutManager = LinearLayoutManager(ctx)
                        }

                        progressBar {
                            id = R.id.progressBarPlayerId
                        }.lparams {
                            centerHorizontally()
                        }
                    }
                }
            }
        }
    }

}
