package dicoding.kade.sub5footballapp.match

import android.util.Log
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.Schedule
import dicoding.kade.sub5footballapp.model.ScheduleResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MatchPresenter(private val view: MatchView,
                     private val apiClient: ApiClient) {

    fun getMatchList(mode: String?, leagueName: String?) {
        view.showLoading()

        val scheduleId: String = when (mode) {
            "prevMatch" -> "eventspastleague.php"
            "nextMatch" -> "eventsnextleague.php"
            else -> ""
        }

        val leagueId: String = when (leagueName){
            "English Premier League" -> "4328"
            "German Bundesliga" -> "4331"
            "Italian Serie A" -> "4332"
            "Spanish La Liga" -> "4335"
            else -> "4328"
        }

        val cek: Call<ScheduleResponse> = apiClient.get().getSchedule(scheduleId, leagueId)
        cek.enqueue(object : Callback<ScheduleResponse> {
            override fun onResponse(call: Call<ScheduleResponse>, response: Response<ScheduleResponse>) {
                Log.d("Retrofit", "success")
                if (response.isSuccessful && response.body() != null) {
                    Log.d("Retrofit2", response.body().toString())
                    val listResponse: List<Schedule>? = response.body()!!.schedules
                    Log.d("Retrofit3", listResponse.toString())
                    view.hideLoading()
                    if (listResponse != null) view.showMatchList(listResponse)
                }
            }

            override fun onFailure(call: Call<ScheduleResponse>?, t: Throwable?) {
                Log.d("Retrofit", "failure : " + t?.message)
                view.hideLoading()
            }
        })

    }

}