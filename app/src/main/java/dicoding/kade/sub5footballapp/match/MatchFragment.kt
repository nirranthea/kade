package dicoding.kade.sub5footballapp.match


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.color.colorAccent
import dicoding.kade.sub2footballmatchschedule.R.id.*
import dicoding.kade.sub5footballapp.adapter.MatchAdapter
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.detail.DetailMatchActivity
import dicoding.kade.sub5footballapp.model.Schedule
import dicoding.kade.sub5footballapp.utils.invisible
import dicoding.kade.sub5footballapp.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.*

class MatchFragment : Fragment(), MatchView {

    private val schedules: MutableList<Schedule> = mutableListOf()
    private var mode: String? = ""
    private lateinit var listAdapter: MatchAdapter
    private lateinit var ankoUI: MatchFragmentUI
    private lateinit var presenter: MatchPresenter
    private lateinit var progBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private var leagueName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mode = arguments?.getString("mode")
    }

    companion object {
        fun newInstance(mode: String): MatchFragment {
            val args = Bundle()
            args.putString("mode", mode)
            val fragment = MatchFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = MatchFragmentUI().createView(AnkoContext.create(ctx, this))

        ankoUI = MatchFragmentUI()
        //TODO : setup RecyclerView Adapter & send Extras through Intent
        listAdapter = MatchAdapter(schedules, mode!!) {
            startActivity<DetailMatchActivity>(
                    "teamHome" to "${it.teamHome}",
                    "teamAway" to "${it.teamAway}",
                    "scoreHome" to it.scoreHome,
                    "scoreAway" to it.scoreAway,
                    "date" to "${it.date}",
                    "time" to "${it.time}",
                    "teamHomeId" to "${it.teamHomeId}",
                    "teamAwayId" to "${it.teamAwayId}",
                    "mode" to "$mode",
                    "eventId" to "${it.eventId}",
                    "goalsHome" to "${it.goalsHome}",
                    "goalsAway" to "${it.goalsAway}",
                    "redCardsHome" to "${it.redCardsHome}",
                    "redCardsAway" to "${it.redCardsAway}",
                    "yellowCardsHome" to "${it.yellowCardsHome}",
                    "yellowCardsAway" to "${it.yellowCardsAway}",
                    "shotsHome" to it.shotsHome,
                    "shotsAway" to it.shotsAway,
                    "goalKeeperHome" to "${it.goalKeeperHome}",
                    "goalKeeperAway" to "${it.goalKeeperAway}",
                    "defenseHome" to "${it.defenseHome}",
                    "defenseAway" to "${it.defenseAway}",
                    "midfieldHome" to "${it.midfieldHome}",
                    "midfieldAway" to "${it.midfieldAway}",
                    "forwardHome" to "${it.forwardHome}",
                    "forwardAway" to "${it.forwardAway}"
            )
        }

        //TODO : initialize Spinner in ANKO fragment
        val spinner: Spinner = view.findViewById(spinnerId)
        val spinnerItems = resources.getStringArray(R.array.league_name)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long){
                leagueName = spinner.selectedItem.toString()
                presenter.getMatchList(mode,leagueName)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                leagueName = resources.getStringArray(R.array.league_name)[0]
            }
        }

        //TODO: initialize RecyclerView in ANKO fragment
        val rv: RecyclerView = view.findViewById(recyclerViewId)
        rv.adapter = listAdapter
        rv.contentDescription = mode

        //TODO : initialize ProgressBar in ANKO fragment
        progBar = view.findViewById(progressBarId)

        //TODO: initialize Presenter
        val request = ApiClient()
        presenter = MatchPresenter(this, request)
        presenter.getMatchList(mode,leagueName)

        //TODO: swipeRefresh behaviour
        swipeRefresh = view.findViewById(swipeRefreshId)
        swipeRefresh.onRefresh {
            presenter.getMatchList(mode,leagueName)
        }

        return view
    }

    //TODO: ANKO-layout for MatchFragment
    class MatchFragmentUI : AnkoComponent<MatchFragment> {
        override fun createView(ui: AnkoContext<MatchFragment>): View = with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL

                spinner{
                    id = spinnerId
                }
                view {
                    setBackgroundResource(R.drawable.shadow)
                }.lparams(width = matchParent, height = dip(4f)) {

                }
                swipeRefreshLayout {
                    id = swipeRefreshId
                    setColorSchemeResources(colorAccent,
                            android.R.color.holo_green_light,
                            android.R.color.holo_orange_light,
                            android.R.color.holo_red_light)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        recyclerView {
                            lparams(width = matchParent, height = wrapContent)
                            id = recyclerViewId
                            layoutManager = LinearLayoutManager(ctx)
                        }

                        progressBar {
                            id = progressBarId
                        }.lparams {
                            centerHorizontally()
                        }
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progBar.visible()
    }

    override fun hideLoading() {
        progBar.invisible()
    }

    //TODO : update List and notify RecyclerView Adapter
    override fun showMatchList(data: List<Schedule>) {
        swipeRefresh.isRefreshing = false
        schedules.clear()
        schedules.addAll(data)
        listAdapter.notifyDataSetChanged()
    }

}