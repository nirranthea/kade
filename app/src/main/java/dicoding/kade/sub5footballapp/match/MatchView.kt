package dicoding.kade.sub5footballapp.match

import dicoding.kade.sub5footballapp.model.Schedule

interface MatchView {

    fun showLoading()
    fun hideLoading()
    fun showMatchList(data: List<Schedule>)

}