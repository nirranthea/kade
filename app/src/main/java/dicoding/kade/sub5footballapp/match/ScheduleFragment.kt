package dicoding.kade.sub5footballapp.match


import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.*
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.display_search
import dicoding.kade.sub2footballmatchschedule.R.menu.menu_search
import dicoding.kade.sub5footballapp.search.SearchActivity
import dicoding.kade.sub5footballapp.adapter.TabScheduleAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.viewPager

//TODO: Migrated from MatchActivity(main) into this Fragment
class ScheduleFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        //TODO : enable optionsMenu in toolbar
        setHasOptionsMenu(true)

        Log.d("Fragment","onCreateView ScheduleFragment")
        val view = ScheduleFragmentUI().createView(AnkoContext.create(ctx, this))
        val pager: ViewPager = view.findViewById(R.id.viewPagerId)

        //TODO: common mistakes (because fragment in fragment should use childFragmentManager)
        pager.adapter = TabScheduleAdapter(childFragmentManager)

        val tab: TabLayout = view.findViewById(R.id.tabLayoutId)
        tab.setupWithViewPager(pager)
        tab.tabGravity = TabLayout.GRAVITY_FILL

        return view
    }

    //TODO : ANKO-Layout for Schedule Fragment
    class ScheduleFragmentUI : AnkoComponent<ScheduleFragment> {
        lateinit var tabMode: TabLayout
        private lateinit var pagerMode: ViewPager

        override fun createView(ui: AnkoContext<ScheduleFragment>) = with(ui) {
            verticalLayout {
                lparams(width = matchParent, height = wrapContent)

                tabMode = tabLayout {
                    lparams(width = matchParent, height = wrapContent)
                    id = R.id.tabLayoutId
                    setTabTextColors(Color.BLACK, Color.WHITE)
                    backgroundColor = ContextCompat.getColor(context, R.color.colorPrimary)
                }
                pagerMode = viewPager {
                    id = R.id.viewPagerId
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }

    //TODO : Inflate menu contains Search
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(menu_search, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            display_search -> {
                Log.d("SearchTest","menu_search clicked")
                startActivity<SearchActivity>(
                        "fragment_type" to "MATCH_FRAGMENT"
                )
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
