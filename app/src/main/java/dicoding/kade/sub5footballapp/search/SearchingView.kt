package dicoding.kade.sub5footballapp.search

import dicoding.kade.sub5footballapp.model.Schedule
import dicoding.kade.sub5footballapp.model.Team

interface SearchingView {

    fun showLoading()
    fun hideLoading()
    fun showSearchMatchList(data: List<Schedule>)
    fun showSearchTeamList(data: List<Team>)

}