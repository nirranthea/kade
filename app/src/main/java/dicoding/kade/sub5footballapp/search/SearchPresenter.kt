package dicoding.kade.sub5footballapp.search

import android.util.Log
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPresenter(private val view: SearchingView,
                      private val apiClient: ApiClient) {

    fun searchList(searchMode: String, query: String) {
        view.showLoading()

        when (searchMode) {
            "MATCH_FRAGMENT" -> doSearchMatch(query)
            "TEAM_FRAGMENT" -> doSearchTeam(query)
        }
    }

    private fun doSearchTeam(query: String) {
        val call: Call<TeamResponse> = apiClient.get().searchTeam(query)
        call.enqueue(object : Callback<TeamResponse> {
            override fun onResponse(call: Call<TeamResponse>, response: Response<TeamResponse>) {
                Log.d("SearchRetrofit", "success")
                if (response.isSuccessful && response.body() != null) {
                    Log.d("SearchRetrofit2", response.body().toString())
                    val listResponse: List<Team>? = response.body()!!.teams
                    Log.d("SearchRetrofit3", listResponse.toString())
                    view.hideLoading()
                    if (listResponse != null) view.showSearchTeamList(listResponse)
                }
            }

            override fun onFailure(call: Call<TeamResponse>?, t: Throwable?) {
                Log.d("SearchRetrofit", "failure : " + t?.message)
                view.hideLoading()
            }
        })
    }


    private fun doSearchMatch(query: String) {
        val call: Call<MatchResponse> = apiClient.get().searchMatch(query)
        call.enqueue(object : Callback<MatchResponse> {
            override fun onResponse(call: Call<MatchResponse>, response: Response<MatchResponse>) {
                Log.d("SearchRetrofit", "success")
                if (response.isSuccessful && response.body() != null) {
                    Log.d("SearchRetrofit2", response.body().toString())
                    val listResponse: List<Schedule>? = response.body()!!.schedules
                    Log.d("SearchRetrofit3", listResponse.toString())
                    view.hideLoading()
                    if (listResponse != null) view.showSearchMatchList(listResponse)
                }
            }

            override fun onFailure(call: Call<MatchResponse>?, t: Throwable?) {
                Log.d("SearchRetrofit", "failure : " + t?.message)
                view.hideLoading()
            }
        })
    }
}

