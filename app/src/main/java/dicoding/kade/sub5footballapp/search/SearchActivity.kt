package dicoding.kade.sub5footballapp.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import dicoding.kade.sub2footballmatchschedule.R
import dicoding.kade.sub2footballmatchschedule.R.id.action_search
import dicoding.kade.sub2footballmatchschedule.R.menu.menu_action_search
import dicoding.kade.sub5footballapp.adapter.MatchAdapter
import dicoding.kade.sub5footballapp.adapter.TeamAdapter
import dicoding.kade.sub5footballapp.api.ApiClient
import dicoding.kade.sub5footballapp.detail.DetailMatchActivity
import dicoding.kade.sub5footballapp.detail.DetailTeamActivity
import dicoding.kade.sub5footballapp.model.Schedule
import dicoding.kade.sub5footballapp.model.Team
import dicoding.kade.sub5footballapp.utils.invisible
import dicoding.kade.sub5footballapp.utils.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class SearchActivity : AppCompatActivity(), SearchingView {

    private var searchType: String = ""
    private var searchHint: String = ""
    private lateinit var searchUI: SearchActivityUI
    private lateinit var searchRecyclerView: RecyclerView
    private lateinit var searchProgressBar: ProgressBar
    private val teams: MutableList<Team> = mutableListOf()
    private val schedules: MutableList<Schedule> = mutableListOf()
    private lateinit var matchAdapter: MatchAdapter
    private lateinit var teamAdapter: TeamAdapter
    private lateinit var presenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchUI = SearchActivityUI()
        searchUI.setContentView(this)

        //TODO: set Action Bar's title & back Button
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //TODO : get Intent's Extra
        val searchIntent = intent
        searchType = searchIntent.getStringExtra("fragment_type")

        //TODO : initialize RecyclerView & ProgressBar
        searchProgressBar = searchUI.progressBar
        searchProgressBar.invisible()
        searchRecyclerView = searchUI.listSearch

        val mode = "MatchSearch"

        //TODO: initialize adapter
        matchAdapter = MatchAdapter(schedules,mode){
            startActivity<DetailMatchActivity>(
                    "teamHome" to "${it.teamHome}",
                    "teamAway" to "${it.teamAway}",
                    "scoreHome" to it.scoreHome,
                    "scoreAway" to it.scoreAway,
                    "date" to "${it.date}",
                    "time" to "${it.time}",
                    "teamHomeId" to "${it.teamHomeId}",
                    "teamAwayId" to "${it.teamAwayId}",
                    "mode" to mode,
                    "eventId" to "${it.eventId}",
                    "goalsHome" to "${it.goalsHome}",
                    "goalsAway" to "${it.goalsAway}",
                    "redCardsHome" to "${it.redCardsHome}",
                    "redCardsAway" to "${it.redCardsAway}",
                    "yellowCardsHome" to "${it.yellowCardsHome}",
                    "yellowCardsAway" to "${it.yellowCardsAway}",
                    "shotsHome" to it.shotsHome,
                    "shotsAway" to it.shotsAway,
                    "goalKeeperHome" to "${it.goalKeeperHome}",
                    "goalKeeperAway" to "${it.goalKeeperAway}",
                    "defenseHome" to "${it.defenseHome}",
                    "defenseAway" to "${it.defenseAway}",
                    "midfieldHome" to "${it.midfieldHome}",
                    "midfieldAway" to "${it.midfieldAway}",
                    "forwardHome" to "${it.forwardHome}",
                    "forwardAway" to "${it.forwardAway}"
            )
        }
        teamAdapter = TeamAdapter(teams){
            startActivity<DetailTeamActivity>(
                    "team_Id" to "${it.teamId}",
                    "team_name" to "${it.teamName}",
                    "team_badge" to "${it.teamBadge}",
                    "team_desc" to "${it.teamDescription}",
                    "team_stadium" to "${it.teamStadium}",
                    "team_year" to "${it.teamFormedYear}")
        }

        //TODO: initialize presenter
        val request = ApiClient()
        presenter = SearchPresenter(this, request)

        when (searchType) {
            "MATCH_FRAGMENT" -> {
                searchRecyclerView.adapter = matchAdapter
                searchHint = "Match"
            }
            "TEAM_FRAGMENT" -> {
                searchRecyclerView.adapter = teamAdapter
                searchHint = "Team"
            }
        }

    }

    class SearchActivityUI : AnkoComponent<SearchActivity> {
        lateinit var listSearch: RecyclerView
        lateinit var progressBar: ProgressBar
        override fun createView(ui: AnkoContext<SearchActivity>): View = with(ui) {

            //TODO: ANKO-layout
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL
                leftPadding = dip(16)
                rightPadding = dip(16)


                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    listSearch = recyclerView {
                        lparams(width = matchParent, height = wrapContent)
                        id = R.id.recyclerViewSearchId
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                        id = R.id.progressBarSearchId
                    }.lparams {
                        centerHorizontally()
                    }
                }

            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(menu_action_search, menu)
        val searchView: SearchView = menu?.findItem(action_search)?.actionView as SearchView
        val searchManager: SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)
        searchView.isIconified = false
        searchView.queryHint = "Search $searchHint..."
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("Search", "ambil $newText")
                Log.d("Search", "searchType $searchType")

                //TODO : delay 2 second on Text Change then call search function
                Handler().postDelayed({
                    presenter.searchList(searchType,newText)
                },2000)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("Search", "ambil $query")
                Log.d("Search", "searchType $searchType")
                presenter.searchList(searchType,query)
                return false
            }
        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showLoading() {
        searchProgressBar.visible()
    }

    override fun hideLoading() {
        searchProgressBar.invisible()
    }

    override fun showSearchMatchList(data: List<Schedule>) {
        schedules.clear()
        schedules.addAll(data)
        matchAdapter.notifyDataSetChanged()
    }

    override fun showSearchTeamList(data: List<Team>) {
        teams.clear()
        teams.addAll(data)
        teamAdapter.notifyDataSetChanged()
    }
}
