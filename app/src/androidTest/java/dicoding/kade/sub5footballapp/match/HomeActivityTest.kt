package dicoding.kade.sub5footballapp.match

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import dicoding.kade.sub2footballmatchschedule.R.id.*
import dicoding.kade.sub5footballapp.HomeActivity
import org.hamcrest.CoreMatchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class HomeActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(HomeActivity::class.java)

        //TODO : Instrumentation Test - Prev.Match Tab & Detail
    @Test
    fun testPrevMatch() {

        //Swipe to right to ensure PREV. MATCH Tab is selected
        onView(withId(viewPagerId)).perform(swipeRight())

        // Wait 3 seconds so RecyclerView finish load data (if internet is slow, increase times)
        Thread.sleep(3000)

        //Check if RecyclerView in PREV. MATCH Tab is displayed
        onView(allOf(withId(recyclerViewId), withContentDescription("prevMatch")))
                .check(matches(isDisplayed()))

        //Scroll RecyclerView into some position
        onView(allOf(withId(recyclerViewId), withContentDescription("prevMatch")))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(5))

        //Click RecyclerView's item in some position
        onView(allOf(withId(recyclerViewId), withContentDescription("prevMatch")))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(5, click()))

        //Check if Detail Page is displayed
        onView(withId(detail_date)).check(matches(isDisplayed()))
        onView(withId(detail_team_home_name)).check(matches(isDisplayed()))
        onView(withId(detail_team_away_name)).check(matches(isDisplayed()))

        //Check the scores of Prev Match must not be "?"
        onView(withId(detail_score_home)).check(matches(not(withText("?"))))
        onView(withId(detail_score_away)).check(matches(not(withText("?"))))

        //Back button is pressed
        Espresso.pressBack()
    }

    //TODO : Instrumentation Test - Next Match Tab & Detail
    @Test
    fun testNextMatch() {

        //Swipe to left so NEXT MATCH Tab is selected
        onView(withId(viewPagerId)).perform(swipeLeft())

        // Wait 3 seconds so RecyclerView finish load data (if internet is slow, increase times)
        Thread.sleep(3000)

        //Check if RecyclerView in NEXT MATCH Tab is displayed
        onView(allOf(withId(recyclerViewId), withContentDescription("nextMatch")))
                .check(matches(isDisplayed()))

        //Scroll RecyclerView into some position
        onView(allOf(withId(recyclerViewId), withContentDescription("nextMatch")))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))

        //Click RecyclerView's item in some position
        onView(allOf(withId(recyclerViewId), withContentDescription("nextMatch")))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        //Check if Detail Page is displayed
        onView(withId(detail_date)).check(matches(isDisplayed()))
        onView(withId(detail_team_home_name)).check(matches(isDisplayed()))
        onView(withId(detail_team_away_name)).check(matches(isDisplayed()))

        //Check the scores of Next Match must be "?"
        onView(withId(detail_score_home)).check(matches(withText("?")))
        onView(withId(detail_score_away)).check(matches(withText("?")))

        //Back button is pressed
        Espresso.pressBack()
    }

    //TODO : Instrumentation Test - add Favorite & delete Favourite
    // (assumed there are no any item in Favorite, if there are any please removed it all first manually)
    @Test
    fun testAddDeleteFavorite() {

        //Swipe to left so NEXT MATCH Tab is selected
        onView(withId(viewPagerId)).perform(swipeLeft())

        // Wait 3 seconds so RecyclerView finish load data (if internet is slow, increase times)
        Thread.sleep(3000)

        //Click RecyclerView's item in some position
        onView(allOf(withId(recyclerViewId), withContentDescription("nextMatch")))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        //Click add to Favorite
        onView(withId(add_to_favorite)).perform(click())

        //Back button is pressed
        Espresso.pressBack()

        //Check if Favorite is added
        onView(withContentDescription("Favorites")).perform(click())
        onView(withId(fav_date)).check(matches(isDisplayed()))

        //Click to delete Favorite
        onView(withId(fav_date)).perform(click())
        onView(withId(add_to_favorite)).perform(click())

        //Back button is pressed
        Espresso.pressBack()

        //Check if Favorite is removed
        onView(withId(fav_date)).check(doesNotExist())
    }
}